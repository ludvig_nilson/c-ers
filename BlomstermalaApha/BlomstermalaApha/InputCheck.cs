﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlomstermalaApha
{
    class InputCheck
    {

        public List<string> TrimStringCommas(string stringToTrim)
        {
            List<string> resultList = stringToTrim.Split(',')
                .ToList<string>();
            return resultList;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringToTrim">lista på Namn och Pris separerade med komma
        /// (namn, pris) separerade med "." .</param>
        /// <returns></returns>
        public List<string> TrimForProductList(string stringToTrim)
        {
            stringToTrim = stringToTrim.Replace(',', '\t');
            List<string> resultList = stringToTrim.Split('.')
                .ToList<string>();
            return resultList;
        }
        /// <summary>
        /// Trimmar en lista av attribut från en produkt. (Namn,Pris,Antal osv)
        /// tar bort kommatecknen
        /// </summary>
        /// <param name="listToTrim">Namn,Antal,Pris,Tillverkare,Energiklass,Beskrivning</param>
        /// <returns>lista där attributen är separerade</returns>
        public List<string> TrimForProductInformation(List<string> listToTrim)
        {
            List<string> trimmedList = new List<string>();
            
            foreach (string productString in listToTrim)
            {
                trimmedList = productString.Split(',').ToList<string>();
            }
            
            return trimmedList;
        }
        /// <summary>
        /// Tar emot fult namn - "asd\t 1436" och tar bort allt efter backslash
        /// </summary>
        /// <param name="prodName">exemplevis "asd\t 1436"</param>
        /// <returns>bara Namnet som det står i databasen</returns>
        public string TrimmedName(string prodName)
        {
            string trimmedName=null;
            //prodName.Split('\\').ToString();
            if (prodName.Contains("\t"))
                trimmedName = prodName.Substring(0, prodName.LastIndexOf("\t"));
            return trimmedName;
        }
    }
}
